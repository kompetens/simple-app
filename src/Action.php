<?php

namespace VivaTech\SimpleApp;

abstract class Action
{
    /**
     * @var array
     */
    private $jsonInput;

    /**
     * Send a "raw" Json response without any formatting or additional structure.
     *
     * @param mixed $data
     */
    protected function json($data)
    {
        Helpers::jsonResponse($data);
    }

    /**
     * @param mixed $payload
     * @param null  $message
     */
    protected function jsonResponse($payload, $message = null)
    {
        $response = [
            'data'   => $payload,
            'status' => 'success',
        ];

        if ($message !== null) {
            $response['message'] = $message;
        }

        Helpers::jsonResponse($response);
    }

    /**
     *
     * @param string $message
     * @param int    $code
     * @param mixed  $payload
     */
    protected function failJson($message, $code = 400, $payload = null)
    {
        $response = [
            'message' => $message,
            'status'  => 'fail',
        ];

        if ($payload !== null) {
            $response['data'] = $payload;
        }

        Helpers::jsonResponse($response, $code);
    }

    /**
     * @param string $message
     * @param int    $code
     * @param mixed  $payload
     */
    protected function errorJson($message, $code = 500, $payload = null)
    {
        $response = [
            'message' => $message,
            'status'  => 'error',
        ];

        if ($payload !== null) {
            $response['data'] = $payload;
        }

        Helpers::jsonResponse($response, $code);
    }

    /**
     * Get any input key (or all input). By default we also trim all strings and
     * convert empty strings to null.
     *
     * @param  string $key
     * @param  mixed  $default
     * @param  bool   $trim
     * @param  bool   $nullEmpty
     *
     * @return mixed
     */
    protected function input($key = null, $default = null, $trim = true, $nullEmpty = true)
    {
        $input = array_merge($this->getParam(), $this->postData());

        if (empty($key)) {
            return $input;
        }

        $value = array_get($input, $key, $default);

        if (is_string($value)) {
            if ($trim) {
                $value = trim($value);
            }
            if ($nullEmpty) {
                $value = strlen($value) > 0 ? $value : null;
            }
        }

        return $value;
    }

    /**
     * Safely get an item from the query parameters
     *
     * @param  string $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    protected function getParam($key = null, $default = null)
    {
        if (empty($key)) {
            return (array) $_GET;
        }

        return rawurldecode(array_get($_GET, $key, $default));
    }

    /**
     * Safely get an item from the Post data input. Works with normal form input
     * as well as when we get a JSON content body. Since we use array_get to read
     * the data the key can use dot-notation, eg. 'foo.bar' to access the key
     * 'bar' inside the array 'foo';
     *
     * @param  string $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    protected function postData($key = null, $default = null)
    {
        // Set the post data
        $data = $_POST;

        // Parse POST data if sent as JSON.
        if (stripos(array_get($_SERVER, 'CONTENT_TYPE'), 'application/json') !== false) {
            $data = $this->getJsonInput();
        }

        if (empty($key)) {
            return (array) $data;
        }

        return array_get($data, $key, $default);
    }

    /**
     * Decode the content body from json into an associative array.
     * The data is cached on this instance so we do not have to read and decode
     * for every single input variable the user reads.
     *
     * @return array
     */
    protected function getJsonInput()
    {
        if (isset($this->jsonInput)) {
            return $this->jsonInput;
        }

        $content = file_get_contents('php://input');

        if (!empty($content)) {
            return $this->jsonInput = json_decode($content, true);
        }

        return $this->jsonInput = [];
    }
}

<?php

namespace VivaTech\SimpleApp;

use MongoDB\Client;
use MongoDB\Database;
use MongoDB\Exception\Exception as MongoException;
use MongoDB\Exception\RuntimeException as MongoRuntimeException;

class DB
{
    /**
     * @var Database
     */
    private static $database;

    /**
     * Helper to quickly get a database collection.
     *
     * @param  string $name
     * @return \MongoDB\Collection
     */
    public static function collection($name)
    {
        return static::getDatabase()->selectCollection($name);
    }

    /**
     * @return Database
     */
    public static function getDatabase()
    {
        if (static::$database instanceof Database) {
            return static::$database;
        }

        if (!defined('DB_NAME') || !defined('DB_HOST')) {
            throw new \RuntimeException('Required constants "DB_HOST" or "DB_NAME" not defined');
        }

        $uriOptions = [
            'db'               => (defined('DB_AUTH') && !empty(DB_AUTH)) ? DB_AUTH : DB_NAME,
            'connect'          => true,
            'connectTimeoutMS' => 5000,
        ];

        // Add replica set settings if defined
        if (defined('DB_REPLICA') && !empty(DB_REPLICA)) {
            $uriOptions['replicaSet'] = DB_REPLICA;
        }

        // Set login information if defined
        if ((defined('DB_USER') && !empty(DB_USER)) && defined('DB_PASS') && !empty(DB_PASS)) {
            $uriOptions['username'] = DB_USER;
            $uriOptions['password'] = DB_PASS;
        }

        $driverOptions = [
            'typeMap' => [
                'root'     => 'array',
                'document' => 'array',
            ],
        ];

        $lastError = null;
        $attempts  = 0;

        while (5 > $attempts++) {

            try {
                $client = new Client(sprintf('mongodb://%s/%s', DB_HOST, DB_NAME), $uriOptions, $driverOptions);

                return static::$database = $client->selectDatabase(DB_NAME);

            } catch (MongoException $e) {
                $lastError = $e->getMessage();
                sleep(2);
            }
        }

        if (empty($lastError)) {
            $lastError = 'Unable to connect to the MongoDB database after ' . $attempts . ' attempts.';
        }

        throw new MongoRuntimeException($lastError);
    }
}

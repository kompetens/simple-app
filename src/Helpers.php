<?php

namespace VivaTech\SimpleApp;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

class Helpers
{
    /**
     * @param mixed $content
     * @param int   $code
     */
    public static function jsonResponse($content, $code = 200)
    {
        $encoded = json_encode(Helpers::serializeMongoVars($content));

        header('Content-Type: application/json', true, $code);

        echo $encoded;
        exit;
    }

    /**
     * Go through all properties of this item and convert MongoDB objects into more readable formats.
     *
     * @param  mixed $item
     * @return array|object
     */
    public static function serializeMongoVars($item)
    {
        if (!is_array($item) && !is_object($item) && !$item instanceof \Traversable) {
            return $item;
        }

        $formatted = [];

        $dateTimezone = defined('CARBON_TIMEZONE') ? CARBON_TIMEZONE : 'Europe/Stockholm';
        $dateFormat   = defined('DATE_SERIALIZATION_FORMAT') ? DATE_SERIALIZATION_FORMAT : 'Y-m-d H:i';

        foreach ((array) $item as $key => $value) {

            if ($value instanceof ObjectID) {
                $formatted[$key] = (string) $value;

            } elseif ($value instanceof UTCDateTime) {
                $formatted[$key] = $value->toDateTime()
                                         ->setTimezone(new \DateTimeZone($dateTimezone))
                                         ->format($dateFormat);

            } elseif (is_array($value) && count($value)) {
                $formatted[$key] = static::serializeMongoVars($value);

            } else {
                $formatted[$key] = $value;
            }
        }

        /**
         * if we received an object we make sure to cast the formatted value back
         * to an object. This way "empty arrays" that should be objects in JS
         * will not be "falsely" encoded as arrays.
         */
        return is_object($item) ? (object) $formatted : $formatted;
    }

    /**
     * Determines if the current request is a Json request
     *
     * @return bool
     */
    public static function isJsonRequest()
    {
        // Check if the request is an AJAX request
        if (strtolower(array_get($_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
            return true;
        }

        // Check if the accept header is present.
        if (str_contains(array_get($_SERVER, 'HTTP_ACCEPT', ''), ['/json', '/javascript'])) {
            return true;
        }

        // Check content type
        if (str_contains(array_get($_SERVER, 'CONTENT_TYPE', ''), ['/json', '/javascript'])) {
            return true;
        }

        return false;
    }
}

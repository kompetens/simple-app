<?php

namespace VivaTech\SimpleApp;

class Response
{
    /**
     * @var bool
     */
    protected $isJson = false;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * HTTP response code to return
     *
     * @var int
     */
    protected $code = 200;

    /**
     * Make a response object.
     * Note that the data must be a string or "cast-able" to a string.
     *
     * @param  mixed $data
     * @param  int   $code
     * @return static
     */
    public static function make($data, $code = 200)
    {
        return (new static())
            ->withData((string) $data)
            ->withCode($code);
    }

    /**
     * @param  mixed $data
     * @param  int   $code
     * @return static
     */
    public static function json($data, $code = 200)
    {
        if (is_resource($data)) {
            throw new \InvalidArgumentException("Data when creating a json response must not be a resource");
        }

        return (new static())
            ->withData($data)
            ->withCode($code)
            ->asJson();
    }

    /**
     * @param  mixed $data
     * @return static
     */
    public function withData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set the HTTP status code
     *
     * @param  int $code
     * @return $this
     */
    public function withCode($code = 200)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * "Send" the response to the user.
     */
    public function render()
    {
        if ($this->isJson) {
            sa_jsonResponse($this->data, $this->code);
        }

        http_response_code($this->code);
        echo $this->data;
        exit;
    }

    /**
     * Set the response to use json as the content type.
     *
     * @return $this
     */
    public function asJson()
    {
        $this->isJson = true;

        return $this;
    }
}

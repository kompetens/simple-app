<?php

namespace VivaTech\SimpleApp;

use Closure;
use FastRoute\Dispatcher;
use VivaTech\SimpleApp\Errors\InvalidMethodException;
use VivaTech\SimpleApp\Errors\NotFoundException;
use VivaTech\SimpleApp\Errors\RouteHandlerException;

/**
 * Class Router
 *
 * @package VivaTech\SimpleApp
 */
class Router
{
    /**
     * The route Dispatcher's status code for the currently matched route.
     *
     * @var int
     */
    protected $routeStatus;

    /**
     * The handler to use for the current route. Contains an array of the allowed
     * request methods if a route was requested with an invalid method.
     *
     * @var mixed
     */
    protected $routeHandler;

    /**
     * Contains the route parameters defined in the route URI.
     *
     * @var array
     */
    protected $routeParams;

    /**
     * Router constructor.
     *
     * @param callable $routes
     */
    public function __construct(callable $routes)
    {
        //  Get the URI of the current request, but exclude the query string.
        $uri = $_SERVER['REQUEST_URI'];
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }

        $uri = rawurldecode($uri);

        // Match the current route
        $routeInfo = \FastRoute\simpleDispatcher($routes)->dispatch($_SERVER['REQUEST_METHOD'], $uri);

        // Set the separate route info parts as class variables.
        $this->routeStatus  = (int) array_get($routeInfo, 0, 0);
        $this->routeHandler = array_get($routeInfo, 1);
        $this->routeParams  = (array) array_get($routeInfo, 2);
    }

    /**
     * Match the route and dispatch immediately if we found a route. We accept
     * two optional callbacks to handle routes not found and method mismatches.
     *
     * @param  Closure $notFound
     * @param  Closure $invalidMethod
     * @throws InvalidMethodException
     * @throws NotFoundException
     */
    public function route(Closure $notFound = null, Closure $invalidMethod = null)
    {
        if ($this->hasMatchedRoute()) {
            $this->callHandler();

        } elseif ($this->routeNotFound()) {
            if (is_callable($notFound)) {
                $notFound();
            } else {
                throw new NotFoundException("Route not found");
            }

        } elseif ($this->invalidMethod()) {
            if (is_callable($invalidMethod)) {
                $invalidMethod();
            } else {
                throw new InvalidMethodException("Invalid request method.");
            }
        }
    }

    /**
     * @return bool
     */
    public function hasMatchedRoute()
    {
        return $this->routeStatus === Dispatcher::FOUND;
    }

    /**
     * Attempt to call the handler for the found route. Valid handlers are fully
     * qualified class names or instances of classes with an __invoke method,
     * anonymous function or a string reference to a function.
     */
    public function callHandler()
    {
        if (is_callable($this->routeHandler)) {
            $this->handleResponse(
                call_user_func_array($this->routeHandler, $this->routeParams)
            );

        } elseif (is_string($this->routeHandler) && class_exists($this->routeHandler)) {
            $this->handleResponse(
                call_user_func_array((new $this->routeHandler), $this->routeParams)
            );
        }

        throw new RouteHandlerException("Cannot dispatch route, no valid route handler found.");
    }

    /**
     * Simple handler for data returned from route handlers. This step is
     * "optional" in the way that the handler might print the response
     * and exit the script before we get here.
     *
     * @param mixed $response
     */
    protected function handleResponse($response = null)
    {
        if (is_string($response)) {
            echo $response;

        } elseif ($response instanceof Response) {
            $response->render();

        } elseif (!empty($response) && !is_resource($response)) {
            sa_jsonResponse($response);
        }

        exit;
    }

    /**
     * @return bool
     */
    public function routeNotFound()
    {
        return $this->routeStatus === Dispatcher::NOT_FOUND;
    }

    /**
     * @return bool
     */
    public function invalidMethod()
    {
        return $this->routeStatus === Dispatcher::METHOD_NOT_ALLOWED;
    }
}

<?php

/**
 * Print a json response with the appropriate headers and exit the script.
 * $content that is serializable. The MongoDB specific types will be
 * automatically serialized properly.
 *
 * @param mixed $content
 * @param int   $code
 */
function sa_jsonResponse($content, $code = 200)
{
    \VivaTech\SimpleApp\Helpers::jsonResponse($content, $code);
}

/**
 * Go through all properties of this item and convert MongoDB objects into more readable formats.
 *
 * @param  mixed $item
 * @return array|object
 */
function sa_serializeMongoVars($item)
{
    return \VivaTech\SimpleApp\Helpers::serializeMongoVars($item);
}

/**
 * Get a database connection or a collection therein directly.
 *
 * @param  string|null $collection
 * @return \MongoDB\Collection|\MongoDB\Database
 */
function sa_db($collection = null)
{
    if (is_string($collection) && !empty($collection)) {
        return \VivaTech\SimpleApp\DB::collection($collection);
    }

    return \VivaTech\SimpleApp\DB::getDatabase();
}

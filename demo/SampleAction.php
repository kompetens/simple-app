<?php

class SampleAction extends \VivaTech\SimpleApp\Action
{
    /**
     * Sample invoke method
     *
     * @param $parameter string   Some parameter passed through the route URI.
     */
    public function __invoke($parameter)
    {
        $dataFromParam = $this->dataFromExtractedMethod($parameter);

        if ($this->postData('another_input') === 'foo') {
            $this->errorJson('You cannot input foo', 422);
        }

        $this->jsonResponse([
            'computed' => $dataFromParam,
        ]);
    }

    /**
     * @param  $parameter
     * @return array
     */
    private function dataFromExtractedMethod($parameter)
    {
        // This is a long method that does something with the parameter.

        // Maybe it checks caching based on current env or something

        // anyway, it was extracted from the invoke method to make the "main method" more readable

        // We needed some post data though.
        $inputData = $this->postData('input_key', 'sensible_default');

        return [
            'input' => $inputData,
            'param' => $parameter,
        ];
    }
}
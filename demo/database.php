<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * Define constants for database settings. Only the host and name are required
 * to get a connection. If the server requires authentication the other
 * constants should also be defined.
 */
define('DB_HOST', 'localhost');
define('DB_NAME', 'database');
//define('DB_USER', '');
//define('DB_PASS', '');
//define('DB_AUTH', ''); // Auth source if other than the database we are connecting to
//define('DB_REPLICA', ''); // Name of replica set

// Insert operation
\VivaTech\SimpleApp\DB::collection('posts')->insertOne([
    'title' => 'Foo Bar',
    'body' => 'Some post body text'
]);

// Read operation
$post = \VivaTech\SimpleApp\DB::collection('posts')->findOne(['_id' => 'some-id']);

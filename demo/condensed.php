<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * Setup the router with all the available routes.
 */
$router = new \VivaTech\SimpleApp\Router(function (\FastRoute\RouteCollector $router) {

    // REST-routes to manage posts
    $router->get('post', 'MyNamespace\\AllPostsClass');
    $router->get('post/create', 'MyNamespace\\CreatePostClass');
    $router->post('post', 'MyNamespace\\StorePostClass');
    $router->get('post/{id}', 'MyNamespace\\ShowPostClass');
    $router->patch('post/{id}', 'MyNamespace\\UpdatePostClass');
    $router->delete('post/{id}', 'MyNamespace\\DeletePostClass');

});

// Optionally wrap this in a try-catch block to catch 404, 405 or other errors.
$router->route(
    function() {
        // Route was not found. optional callback
    },
    function() {
        // invalid route method. Optional callback
    }
);

<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * Sample class handler
 */
class SomeAction extends \VivaTech\SimpleApp\Action
{
    public function __invoke($foo)
    {
        // Do something
    }
}

/**
 * Sample function handler
 *
 * @param string $bar
 */
function sampleFunctionHandler($bar = null)
{
    // Do something
}

/**
 * Setup the router with all the available routes.
 */
$router = new \VivaTech\SimpleApp\Router(function (\FastRoute\RouteCollector $router) {

    $instantiatedAction = new SomeAction();

    /**
     * Register a route using the FQN to a class with an invoke method
     */
    $router->get('action/{foo}', SomeAction::class);

    /**
     * Register a route using an instantiated class as a handler.
     */
    $router->get('instance/{foo}', $instantiatedAction);

    /**
     * Register a route using a string reference to a declared function.
     * Note the optional route parameter in the URI.
     */
    $router->post('function/[{bar}]', 'sampleFunctionHandler');

    /**
     * Declare a route with an inline anonymous function as the handler.
     */
    $router->delete('anonymous', function () {
        // Do something
    });

});

/**
 * Check if we found a route for the current request and call the handler.
 */
if ($router->hasMatchedRoute()) {
    $router->callHandler();
}

if ($router->routeNotFound()) {
    // Render 404-error page or json response
}

if ($router->invalidMethod()) {
    // Incorrect request method was used (eg. POST was used instead of GET).
}
